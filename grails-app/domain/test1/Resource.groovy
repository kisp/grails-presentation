package test1

class Resource {

    static constraints = {
      user nullable: true
      //name matches: /[a-z][.]{2:199}/
      description maxSize: 1000
    }

    static belongsTo = [user:User]

    String name
    String description
}
