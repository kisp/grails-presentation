package test1

class User {

    static constraints = {
      name matches: /[A-Z][a-z].*/
    }
    
    static hasMany = [resources:Resource]

    String name

}
