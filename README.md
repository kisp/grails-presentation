# Grails Workshop

This file contains my history

1. grails create-app test1
2. sniffing around

* mkdir grails_docs
* grails help > grails_docs/commands.txt
* grails list-plugins > grails_docs/list-plugins.txt

3. choosing plugins

* authentication
* scaffolding
* mongodb
* routing
* heroku

4. Using interactive command line 

start with the command grails!
- same JVM, speeds up execution of commands

$ grails
    grails> help
      ...
    grails> shell
      ... compiling, compiling 
      got a groovy shell

    grails> plugin-info authentication
      ...
    grails> plugin-info scaffolding
      ...
    grails> plugin-info mongodb
    grails> plugin-info routing
    grails> plugin-info heroku
      ... maybe I do not need these

    grails> help generate-all 
      ... I will definetly need this!

    grails> url-mappings-report
      ... > url map
    grails> install-plugin authentication
      ... unable to do this in interactive mode :(
    grails> help install-app-templates
      ???
    grails> help integrate-with
      ... > you can integrate with an IDE!

$ grails install-plugin authentication

| Warning
Since Grails 2.3, it is no longer possible to install plugins using the install-plugin command.
Plugins must be declared in the grails-app/conf/BuildConfig.groovy file.

Example:
grails.project.dependency.resolution = {
   ...
   plugins {
      compile ":authentication:2.0.1"
   }
}

View and modify BuildConfig as suggested

    grails> compile

$ git st
 ... > nothing is modificated

 Start the app to check authentication, what is it good for??!

    grails> run-app

    ... > Loaded in intercative mode! check localhost:8080/test1

Ok, got the concept we do not have anything special here, so 
get a real authentication plugin:

http://grails.org/plugin/spring-security-ui

Lets add dependencies for it, but first stop the server!

    grails> stop-app

 Trying install-templates
    grails> install-templates

    ... created template files. --> stored it on a separated branch!

Lets start doing things!
Focusing on
1. Authentication 
2. CRUD new objects with command generate-all MyResources

Adding some sugar:
        compile ":spring-security-core:2.0-RC2"
        compile ":mail:1.0.1"
        compile ":jquery:1.10.2"
        compile ":jquery-ui:1.10.2"
        compile ":famfamfam:1.0.1"
    grails> refresh-dependencies
    grails> dependency-report
    grails> run-app
    

    ----

    generating stuff:

    grails> create-domain-class User
    grails> create-domain-class Resource
    ... examine new files.

    Object relational mapping:

    * http://grails.org/doc/latest/guide/GORM.html
    one-to-many relationship

Adding compile:scaffolding to plugins BuildConfig.groovy
Executed:
    $ grails compile

(interactive mode did not work )

    ---------

    Test domain objects:

    grails> create-hibernate-cfg-xml

    grails> create-scaffold-controller test1.Resource
    grails> create-scaffold-controller test1.User


    TODO:
    
    grails> stats

    see table, showing number of files, tests 
    
    tests
    -----

    grails> test-app
    | Running without daemon...
    | Compiling 2 source files
    | Compiling 2 source files.
    | Running 2 unit tests...
    | Completed 0 unit test, 0 failed in 0m 23s
    | Tests PASSED - view reports in C:\DEV\projects\grails\test1\target\test-reports

    examine files:


    test / SHOW MIXINS!!!
